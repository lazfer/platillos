import { Platillo } from '../../models/platillo';
import { ApiServicesService } from '../../services/api-services.service';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ModalComponent } from 'src/app/components/modal/modal.component';
import { platilloRamdom } from 'src/app/models/platilloRandom';
import { Router } from '@angular/router';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  platillos: Platillo[] = [];
  platilloRandom: platilloRamdom[] = [];



  constructor(private sv: ApiServicesService,
    public dialog: MatDialog,
    private router: Router) { }

  ngOnInit(): void {
    this.sv.getPlatillos().subscribe(
      res => {
        let i = 0;
        res.forEach((ele:any) => {
          i++;
          if(i <= 6 ){
            this.platillos.push(ele);
          }

        });
      }
    )

    this.sv.getRandom().subscribe(
      (res: any) => {
        this.platilloRandom = res;
        this.openDialog();
      }
    )

  }

  detalle(id:any){
    this.router.navigate(["dashboard/detalle" ,id.idMeal ]);
  }

  openDialog(){
    const dialogRef = this.dialog.open(ModalComponent, {
      width: '350px',
      data: {
        id: this.platilloRandom[0].idMeal,
        title: this.platilloRandom[0].strMeal,
        img: this.platilloRandom[0].strMealThumb
      }
    });

    dialogRef.afterClosed().subscribe( res => {
    })
  }

}
