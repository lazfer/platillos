import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Platillo } from 'src/app/models/platillo';
import { ApiServicesService } from 'src/app/services/api-services.service';

@Component({
  selector: 'app-platillos',
  templateUrl: './platillos.component.html',
  styleUrls: ['./platillos.component.css']
})
export class PlatillosComponent implements OnInit {

  platillos: Platillo[] = [];

  constructor(private actRoute: ActivatedRoute, private sv: ApiServicesService,private router: Router) { }

  ngOnInit(): void {

    this.actRoute.params.subscribe(
      res => {
        if(Object.keys(res).length === 0){
          this.loadPlatillos()
        } else {
          this.buscarPlatillos(res.id);
        }

      }
    )

  }

  buscarPlatillos(id: any){
 this.sv.getIngredientePlatilllos(id).subscribe(
   res => {
    this.platillos = res;
   }
 )
  }

  detalle(id:any){
    this.router.navigate(["dashboard/detalle" ,id.idMeal ]);
  }

  loadPlatillos(){
    this.sv.getPlatillos().subscribe(
      res => {
        this.platillos = res;
      }
    )

  }

}
