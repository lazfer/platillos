import { ModalDetalleComponent } from './../../components/modal-detalle/modal-detalle.component';
import { ApiServicesService } from './../../services/api-services.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';


@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.component.html',
  styleUrls: ['./detalle.component.css']
})
export class DetalleComponent implements OnInit {


  detalle: any = null;


  constructor(private actRoute: ActivatedRoute, private sv: ApiServicesService,  public dialog: MatDialog) { }

  ngOnInit(): void {
    this.actRoute.params.subscribe(
      res => {

        this.buscarPlatillo(res.id);
      }
    )
  }

  buscarPlatillo(id:string){
    this.sv.getBuscarId(id).subscribe( res => {
      this.detalle = res[0];
      this.openDialog(res[0]);
    })
  }

  openDialog(detalle:any){
    const dialogRef = this.dialog.open(ModalDetalleComponent, {
      width: '350px',
      data: {
        id: detalle.idMeal,
        title: detalle.strMeal,
        inst: detalle.strInstructions,
        img: detalle.strMealThumb
      }
    });

    dialogRef.afterClosed().subscribe( res => {
    })
  }


}
