import { Router } from '@angular/router';
import { Component, Inject, OnInit } from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {

  constructor( public dialogRef: MatDialogRef<ModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private router: Router) { }

  ngOnInit(): void {
  }

  onNoClick(): void{
    this.dialogRef.close();
     }

     detalle(id:string){
   this.router.navigate(["dashboard/detalle" ,id ]);
   this.dialogRef.close();
     }

}
